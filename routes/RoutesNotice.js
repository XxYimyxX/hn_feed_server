const express  = require('express');
const router = express.Router();
const noticeController = require('../controller/noticeController');

//Obtener la lista de las noticias favoritas.

// -> localhost:4000/api/noticia 
router.get('/', 
    noticeController.favoritoNoticia
);

module.exports = router;
const { request } = require('express');
const express = require('express');
const contextDb = require('./config/db');
const app = express();
const PORT = process.env.PORT || 4000  ;
//Conectarse a la base de dato
contextDb();
//router
app.use('/api/noticia', require('./routes/RoutesNotice'));

app.use(express.json({
  extendes: true
  }));

app.listen(PORT, () => {
  console.log(`Example app listening at http://localhost:${PORT}`)
})

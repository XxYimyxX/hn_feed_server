const mongoose = require('mongoose');

require('dotenv').config({
    path:  'variable.env'
});

const contextDb = async () =>{
    try {
       await mongoose.connect(process.env.DB_MONGO,{
           useNewUrlParser :true ,
           useUnifiedTopology : true,
           useFindAndModify : false
       });
       console.log('se conecto a la base de datos en mongo...')
    } catch (e) {
        console.log(e);
        process.exit(1);
    }
}

module.exports = contextDb ;